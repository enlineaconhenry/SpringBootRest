package com.hcoarite.rest.api.service;

import java.util.List;

import com.hcoarite.rest.api.entity.Course;

public interface CourseService {
	
	public Course createCourse(Course course);

	public List<Course> getAllCourse();

	public Course updateCourse(Integer id, Course course);

	public void deleteCourse(Integer id);
	
	public String findDescById(Integer id);
}
