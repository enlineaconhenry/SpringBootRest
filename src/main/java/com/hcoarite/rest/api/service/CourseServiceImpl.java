package com.hcoarite.rest.api.service;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcoarite.rest.api.entity.Course;
import com.hcoarite.rest.api.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	private CourseRepository courseRepository;

	@Override
	public Course createCourse(Course course) {
		return courseRepository.save(course);
	}

	@Override
	public List<Course> getAllCourse() {
		return courseRepository.findAll();
	}

	@Override
	public Course updateCourse(Integer id, Course course) {
		Course c = courseRepository.findById(id).orElse(new Course());
		c = course;
		c.setIdCourse(id);
		courseRepository.save(course);
		return courseRepository.save(course);
	}

	@Override
	public void deleteCourse(Integer id) {
		courseRepository.deleteById(id);
	}

	@Override
	public String findDescById(Integer id) {
		Optional<Course> list = courseRepository.findById(id);
		if (!list.isEmpty()) {
			Optional<Course> course = list.stream().findFirst();
			return course != null ? course.get().getName() : "S/R";
		} else
			return "S/R";
	}
}
