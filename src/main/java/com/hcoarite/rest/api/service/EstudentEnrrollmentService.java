package com.hcoarite.rest.api.service;

import java.util.List;

import com.hcoarite.rest.api.entity.StudentEnrollment;

public interface EstudentEnrrollmentService {
	
	public StudentEnrollment createStudentEnrollment(StudentEnrollment StudentEnrollment);

	public List<StudentEnrollment> getAllStudentEnrollment();

	public StudentEnrollment updateStudentEnrollment(Integer id, StudentEnrollment StudentEnrollment);

	public void deleteStudentEnrollment(Integer id);
}
