package com.hcoarite.rest.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcoarite.rest.api.entity.Tuition;
import com.hcoarite.rest.api.repository.TuitionRepository;

@Service
public class TuitionServiceImpl implements TuitionService {

	@Autowired
	private TuitionRepository tuitionRepository;

	@Override
	public Tuition createTuition(Tuition tuition) {
		return tuitionRepository.save(tuition);
	}

	@Override
	public List<Tuition> getAllTuition() {
		return tuitionRepository.findAll();
	}

	@Override
	public Tuition updateTuition(Integer id, Tuition tuition) {
		Tuition t = tuitionRepository.findById(id).orElse(new Tuition());
		t = tuition;
		t.setIdTuition(id);
		tuitionRepository.save(tuition);
		return tuitionRepository.save(tuition);
	}

	@Override
	public void deleteTuition(Integer id) {
		tuitionRepository.deleteById(id);
	}

}
