package com.hcoarite.rest.api.service;

import java.util.List;

import com.hcoarite.rest.api.entity.Tuition;

public interface TuitionService {
	
	public Tuition createTuition(Tuition tuition);

	public List<Tuition> getAllTuition();

	public Tuition updateTuition(Integer id, Tuition tuition);

	public void deleteTuition(Integer id);
}
