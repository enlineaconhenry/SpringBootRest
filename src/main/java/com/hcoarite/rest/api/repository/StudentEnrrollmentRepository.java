package com.hcoarite.rest.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.hcoarite.rest.api.entity.Course;
import com.hcoarite.rest.api.entity.StudentEnrollment;

public interface StudentEnrrollmentRepository extends JpaRepository<StudentEnrollment, Integer	>{

}

