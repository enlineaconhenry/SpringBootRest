package com.hcoarite.rest.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.hcoarite.rest.api.entity.Tuition;

public interface TuitionRepository extends JpaRepository<Tuition, Integer>{

}

