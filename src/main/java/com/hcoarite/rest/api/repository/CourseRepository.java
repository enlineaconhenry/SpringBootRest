package com.hcoarite.rest.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.hcoarite.rest.api.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Integer	>{

}

