package com.hcoarite.rest.api.entity;

import java.util.ArrayList;
import java.util.List;

import com.hcoarite.rest.ArrayJsonConverter;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "student_enrollment")
public class StudentEnrollment {
	@Id
	@Column(name = "id_student_enrollment")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idStudentEnrollment;

	@ManyToOne
	@JoinColumn(name = "fk_student", nullable = false, foreignKey = @ForeignKey(name = "id_stdudent"))
	private Student student;

	//Sin soporte para oracle
    /*@CollectionTable(name = "list", joinColumns = @JoinColumn(name = "id_course"))
    @Convert(converter = ArrayJsonConverter.class)
	@Column(name = "list_course",columnDefinition = "json")
	private List<Integer> listCourse = new ArrayList<Integer>();*/
	
    
    @Column(length = 50, nullable = true,name = "list_pk")
	private String listPk;
    
	@Column(length = 50, nullable = true)
	private String classRoom;
}
