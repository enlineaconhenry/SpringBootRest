package com.hcoarite.rest.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcoarite.rest.api.entity.Tuition;
import com.hcoarite.rest.api.service.TuitionService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/rest/tuition")
public class TuitionController {

	@Autowired
	private TuitionService tuitionService;

	@PostMapping("/save")
	public ResponseEntity<Tuition> createTuition(@Valid @RequestBody Tuition Tuition) {

		return new ResponseEntity<Tuition>(tuitionService.createTuition(Tuition), HttpStatus.CREATED);
	}

	@GetMapping("/list")
	public ResponseEntity<List<Tuition>> getAllTuition() {

		return new ResponseEntity<List<Tuition>>(tuitionService.getAllTuition(), HttpStatus.OK);
	}

	@PutMapping("/edit/{id}")
	public ResponseEntity<Tuition> updateTuition(@PathVariable("id") Integer id, @RequestBody Tuition Tuition) {
		return new ResponseEntity<Tuition>(tuitionService.updateTuition(id, Tuition), HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<HttpStatus> deleteTuition(@PathVariable("id") Integer id) {
		tuitionService.deleteTuition(id);
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
}
