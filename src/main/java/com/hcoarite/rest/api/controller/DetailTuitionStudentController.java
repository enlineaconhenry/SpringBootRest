package com.hcoarite.rest.api.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcoarite.rest.ResultAux;
import com.hcoarite.rest.api.entity.Student;
import com.hcoarite.rest.api.entity.StudentEnrollment;
import com.hcoarite.rest.api.service.CourseService;
import com.hcoarite.rest.api.service.EstudentEnrrollmentService;
import com.hcoarite.rest.api.service.StudentService;

@RestController
@RequestMapping("/rest/student")
public class DetailTuitionStudentController {

	@Autowired
	private StudentService studentService;

	@Autowired
	private EstudentEnrrollmentService enrrollmentService;
	
	@Autowired
	private CourseService courseService;

	@GetMapping("/list/order")
	public ResponseEntity<List<Student>> getListStudentOrder() {
		List<Student> list = studentService.getAllStudent();

		Collections.sort(list, new Comparator<Student>() {
			@Override
			public int compare(Student s1, Student s2) {
				return s1.getAge() - s2.getAge();
			}
		});
		return new ResponseEntity<List<Student>>(list, HttpStatus.OK);
	}

	@GetMapping("/list/detail")
	public ResponseEntity<List<ResultAux>> getListStudentDetail() {
		List<StudentEnrollment> list = enrrollmentService.getAllStudentEnrollment();
		
		List<ResultAux> listStudent = new ArrayList<>();
		ResultAux rAux = new ResultAux();
		List<String> aux = new ArrayList<String>();
        for (StudentEnrollment sTemp : list) {
            String[] listCourse = sTemp.getListPk().split(",");
            rAux.setStudenntName(sTemp.getStudent().getName());
            if(listCourse!=null)
            for (String string : listCourse) {
				String couseDesc = courseService.findDescById(Integer.parseInt(string));
				aux.add(couseDesc);
			}
            rAux.setCourseList(aux);
            listStudent.add(rAux);
		}
		return new ResponseEntity<List<ResultAux>>(listStudent, HttpStatus.OK);
	}
}
